#Partimos de ubuntu la última versión
FROM ubuntu:latest

MAINTAINER Louis Perdomo "louis.perdomo79@gmail.com"

#instalamos Python 2.7
RUN apt-get update -qy \
    && apt-get install -y python2.7 python-pip

#El directorio de trabajo será /root
WORKDIR /root