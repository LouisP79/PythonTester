# PythonTester

imagen de Doker con lo necesario para ejecutar el proyecto [TestCICDGitLab](https://gitlab.com/LouisP79/TestCICDGitLab)

El proyecto posee solo el dokerfile con los procesos necesarios

# Información del `Dokerfile`
```
#Partimos de ubuntu la última versión
FROM ubuntu:latest

MAINTAINER Louis Perdomo "louis.perdomo79@gmail.com"

#instalamos Python 2.7
RUN apt-get update -qy \
    && apt-get install -y python2.7 python-pip

#El directorio de trabajo será /root
WORKDIR /root


```

# Código de .gitlab-ci.yml
```yaml
image: docker:latest

services:
  - docker:dind

build:
  stage: build
  script:
    - docker build -t="pythontester:1.0" .

```

# Estado
> En Desarrollo :computer: